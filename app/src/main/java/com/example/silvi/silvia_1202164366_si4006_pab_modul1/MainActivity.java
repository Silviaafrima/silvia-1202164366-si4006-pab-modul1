package com.example.silvi.silvia_1202164366_si4006_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText nilai_alas;
    EditText nilai_tingi;
    TextView nilai_hasil;
    Button hitung;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nilai_alas=(EditText) findViewById(R.id.alas);
        nilai_tingi=(EditText) findViewById(R.id.tinggi);
        nilai_hasil=(TextView) findViewById(R.id.hasil);
        hitung=(Button) findViewById(R.id.cek);

        hitung.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                Float inputvalue= Float.parseFloat(nilai_alas.getText().toString());
                Float inputvalue1= Float.parseFloat(nilai_tingi.getText().toString());
                Float hasil = (inputvalue*inputvalue1);
                nilai_hasil.setText(""+hasil);
            }});
    }
}
